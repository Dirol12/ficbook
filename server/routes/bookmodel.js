class BookModel {
    constructor(title, author) {
        this.title = title;
        this.author = author;
    }
}
module.exports = BookModel;
