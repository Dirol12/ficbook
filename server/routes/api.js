var express = require('express');
var router = express.Router();
const BookRepository = require('./bookrepository.js');

router.get('/books', function(req, res) {
    const newRepository = new BookRepository();
    if (req.query && req.query.q) {
        newRepository.getBooks().then((arrBooks) => {
            const filteredBooks = arrBooks
                .filter((book) => contains(book.title, req.query.q));
            res.json(filteredBooks);
        });
    }
    else {
        newRepository.getBooks().then((arrBooks) => res.json(arrBooks));
    }
});

function contains(str, substr) {
    return str.indexOf(substr) !== -1;
}

module.exports = router;
