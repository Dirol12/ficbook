const BookModel = require('./bookmodel.js');
var path = require('path');
var fs = require('fs');

class BookRepository {
    getBooks() {
        return new Promise((resolve, reject) => {
            const booksPath = path.join(__dirname, '../..', 'books.json');
            console.log(booksPath);
            fs.readFile(booksPath, (err, data) => {
                if (err) {
                    reject(err);
                }
                const tempBooks = JSON.parse(data);
                const arrBooks = tempBooks.map((rawBook) => {
                    return new BookModel(rawBook.title, rawBook.author);
                });
                resolve(arrBooks);
            });
        });
    }
}
module.exports = BookRepository;
