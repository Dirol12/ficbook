const gulp = require('gulp');
const eslint = require('gulp-eslint');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var gutil = require('gulp-util');
var dependencies = ['react','react-dom','jquery'];
var scriptsCount = 0;

gulp.task('scripts', function () {
    bundleApp(false);
});

gulp.task('lint', function() {
    return gulp.src(['./server/**/*.js', './client/**/*.jsx?', 'gulpfile.js'])
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());
});

gulp.task('deploy', function () {
    bundleApp(true);
});

gulp.task('watch', function () {
    gulp.watch(['./client/**/*.jsx?'], ['scripts']);
});

gulp.task('default', ['scripts','watch']);

function bundleApp(isProduction) {
    scriptsCount++;
    var appBundler = browserify({
        entries: './client/app.jsx',
        debug: true
    })

    if (!isProduction && scriptsCount === 1) {
        browserify({
            require: dependencies,
            debug: true
        })
            .bundle()
            .on('error', gutil.log)
            .pipe(source('vendors.js'))
            .pipe(gulp.dest('public/javascripts'));
    }
    if (!isProduction) {
        dependencies.forEach(function(dep) {
            appBundler.external(dep);
        })
    }

    appBundler
        .transform('babelify', {presets: ['es2015', 'react']})
        .bundle()
        .on('error',gutil.log)
        .pipe(source('bundle.js'))
        .pipe(gulp.dest('public/javascripts'));
}
