const React = require('react');
const PropTypes =require('prop-types');

class Book extends React.Component {
    render() {
        return (<li>{this.props.data.title}, {this.props.data.author}</li>);
    }
}
Book.propTypes = {
    data: PropTypes.shape({
        title: PropTypes.string.isRequired,
        author: PropTypes.string.isRequired,
    }),
};
module.exports = Book;
