const React = require('react');
const PropTypes =require('prop-types');

class BooklistSearch extends React.Component {
    render() {
        return(<div>
            <input type="text" size="40" id="booksearch"/>
            <button onClick = {() => {this.props.loadItems(document.getElementById("booksearch").value)}}> Поиск </button>
        </div>);
    }
}

BooklistSearch.propTypes = {
    loadItems: PropTypes.func.isRequired,
};

module.exports = BooklistSearch;
