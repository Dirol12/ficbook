const React = require('react');
const ReactDOM = require('react-dom');
const jquery = require('jquery');
const Booklist = require('./booklist.jsx');

jquery(document).ready(function() {
    ReactDOM.render(
        <Booklist/>,
        document.getElementById('root')
    );
});
