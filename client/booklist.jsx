const React = require('react');
const BooklistSearch = require('./booklistsearch.jsx');
const Book = require('./book.jsx');
const jquery = require('jquery');

class Booklist extends React.Component {
    constructor(props) {
        super(props);
        this.state = { data: null };
    }
    loadItems(str) {
        const url = this.buildURL(str);
        jquery.getJSON(url).done((data) => {
            this.setState({ data: data });
        });
    }

    buildURL (str) {
        return ("/api/books?q=" + str);
    }

    componentDidMount()  {
        return this.loadItems("");
    }

    render() {
        if(this.state.data)
            return (<div><BooklistSearch loadItems = {this.loadItems.bind(this)}/><ul>{this.state.data.map((item) => <Book key={item.title + "-" + item.author } data={item}/>)}</ul></div>);
        return (<div>Loading...</div>);
    }
}
module.exports = Booklist;
