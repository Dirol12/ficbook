const fs = require('fs')
const path = require('path')

fs.createReadStream(path.join(__dirname, 'pre-commit'))
  .pipe(fs.createWriteStream(path.join(__dirname, '..', '.git', 'hooks', 'pre-commit')))
