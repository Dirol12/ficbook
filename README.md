# README #
Site with the possibility of adding your fanfiction work.

# MPV Plans #
1. Adding works with the possibility of choosing criteria that describe the work.
2. Registration users who can to add their work. No registered users can only watch works and leave noname feedbacks. Unlike them registered users can evaluate work, put it in bookmarks ( list will be replenish)
3. The text is added in the text editing window (separate text label for the name, checkboxes for selecting genres, granting another user the rights to edit the text - search for this user by IDE). The font is standard for all works, there is a possibility of italic, bold, or crossed out. Placement on the right (citations), centered (title), left (plain text). 
4. View mode - a work header with a brief description, selected genres, author, etc., the text itself, a feedback field.
5. Search system - selection of the fandom, a separate window with a list of genres, warnings, sorting by the number of reviews, the date of addition.
6. Possibility to save work in txt-format.