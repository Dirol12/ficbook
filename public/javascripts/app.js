'use strict';

$(document).ready(function () {
    ReactDOM.render(React.createElement(Booklist, null),
    // TODO: Why use document.getElementById when you have jQuery?
    // Switch to jQuery
    document.getElementById('root'));
});
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

//TODO: Indent me properly
var Booklist = function (_React$Component) {
    _inherits(Booklist, _React$Component);

    function Booklist(props) {
        _classCallCheck(this, Booklist);

        var _this = _possibleConstructorReturn(this, (Booklist.__proto__ || Object.getPrototypeOf(Booklist)).call(this, props));

        _this.state = { data: null };
        return _this;
    }

    // TODO: Load items should accept filter by default, not an entire url.


    _createClass(Booklist, [{
        key: "loadItems",
        value: function loadItems(url) {
            var _this2 = this;

            $.getJSON(url).done(function (data) {
                // TODO: Use property shorthands from ES6
                // See: http://es6-features.org/#PropertyShorthand
                _this2.setState({ data: data });
            });
        }
    }, {
        key: "componentDidMount",
        value: function componentDidMount() {
            this.loadItems("/api/books");
        }
    }, {
        key: "onSearchChanged",
        value: function onSearchChanged(searchstr) {
            // TODO: Why do you need a vaiable here? just return.
            // TODO: Make it so loadItems builds the url. You just have to pass
            // filter into it
            var searchPath = "/api/books?q=" + searchstr;
            this.loadItems(searchPath);
        }
    }, {
        key: "render",
        value: function render() {
            // Either remove the ugly brackets or use them everywhere - consistently
            if (this.state.data) {
                return React.createElement(
                    "div",
                    null,
                    React.createElement(BooklistSearch, { onSearchChanged: this.onSearchChanged.bind(this) }),
                    React.createElement(
                        "ul",
                        null,
                        this.state.data.map(function (item) {
                            return React.createElement(Book, { data: item });
                        })
                    )
                );
            }
            return React.createElement(
                "div",
                null,
                "Loading..."
            );
        }
    }]);

    return Booklist;
}(React.Component);

// TODO: Move me to another file


var Book = function (_React$Component2) {
    _inherits(Book, _React$Component2);

    function Book() {
        _classCallCheck(this, Book);

        return _possibleConstructorReturn(this, (Book.__proto__ || Object.getPrototypeOf(Book)).apply(this, arguments));
    }

    _createClass(Book, [{
        key: "render",
        value: function render() {
            return React.createElement(
                "li",
                null,
                this.props.data.title,
                ", ",
                this.props.data.author
            );
        }
    }]);

    return Book;
}(React.Component);

// TODO: Move me to another file


var BooklistSearch = function (_React$Component3) {
    _inherits(BooklistSearch, _React$Component3);

    function BooklistSearch() {
        _classCallCheck(this, BooklistSearch);

        return _possibleConstructorReturn(this, (BooklistSearch.__proto__ || Object.getPrototypeOf(BooklistSearch)).apply(this, arguments));
    }

    _createClass(BooklistSearch, [{
        key: "render",
        value: function render() {
            var _this5 = this;

            // Why do you need a vaiable here? just return.
            var x = React.createElement(
                "div",
                null,
                React.createElement("input", { type: "text", size: "40", id: "booksearch" }),
                React.createElement(
                    "button",
                    { onClick: function onClick() {
                            _this5.props.onSearchChanged(document.getElementById("booksearch").value);
                        } },
                    " \u041F\u043E\u0438\u0441\u043A "
                )
            );
            return x;
        }
    }]);

    return BooklistSearch;
}(React.Component);